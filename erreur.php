<?php include(dirname(__FILE__).'/components/header.php'); ?>

<section id="mainsite" class="container-onecolumn">
		<div class="container-typographic">
			<article class="card card-danger">
        <h1 class="card-header"><i class="fa fa-fw fa-error"></i>&nbsp; <?php $plxShow->lang('ERROR'); ?></h1>
				<div class="card-body">
				  <p><?php $plxShow->erreurMessage(); ?></p>
          <p class="align-center"><a href="https://kazhnuz.space" class="btn btn-danger">Retourner à l'accueil</a></p>
				</div>
      </article>
		</div>
</section>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
