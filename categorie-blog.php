<?php include(dirname(__FILE__).'/components/header.php'); ?>

<section id="mainsite" class="container-blog">
	  <section class="sidebar">
			<aside class="card card-info">
	    <div class="card-header"><i class="fa fa-fw fa-info"></i>&nbsp; <?php $plxShow->catName(); ?></div>
				<div class="card-body">
	        <?php $plxShow->catDescription('#cat_description'); ?>
					<p>( <?php $plxShow->artFeed('rss',$plxShow->catId()); ?> )</p>
	      </div>
	    </aside>
	    <?php include(dirname(__FILE__).'/components/sidebar.php'); ?>
	  </section>

		<main class="mainpane">

		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php $plxShow->racine() ?>"><i class="fa fa-fw fa-home"></i>&nbsp;<?php $plxShow->lang('HOME'); ?></a></li><li class="breadcrumb-item" aria-current="page"><span class="active"><?php $plxShow->catName(); ?></span></li>
		  </ol>
		</nav>

    <?php include(dirname(__FILE__).'/components/bloglist.php'); ?>

    <nav class="pagination align-center"><?php $plxShow->pagination(); ?></nav>
		</main>

</section>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
