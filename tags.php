<?php include(dirname(__FILE__).'/components/header.php'); ?>

<section id="mainsite" class="container-blog">
		<main class="mainpane">

		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php $plxShow->racine() ?>"><i class="fa fa-fw fa-home"></i>&nbsp;<?php $plxShow->lang('HOME'); ?></a></li><li class="breadcrumb-item" aria-current="page"><span class="active"><?php $plxShow->tagName(); ?></span></li>
		  </ol>
		</nav>

		<?php include(dirname(__FILE__).'/components/gallery.php'); ?>

			<nav class="pagination align-center"><?php $plxShow->pagination(); ?></nav>
		</main>
	  <div class="sidebar">
			    <aside class="card card-info">
            <div class="card-header"><i class="fa fa-fw fa-info"></i>&nbsp;<?php $plxShow->tagName(); ?></div>
				    <div class="card-body">
	            <p>Toutes les publications contenant le mot-clé « <strong><?php $plxShow->tagName(); ?></strong> »</p>
	            <p>Un mot-clé peut servir à désigner soit l'univers dans laquelle la création se déroule, soit l'objectif dans lequel a été fait la publication (tel que le 100 Thème Challenge)</p>
							<p>( <?php $plxShow->tagFeed() ?> )</p>
	          </div>
	        </aside>

	  		<?php include(dirname(__FILE__).'/components/sidebar.php'); ?>
	  </div>

</section>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
