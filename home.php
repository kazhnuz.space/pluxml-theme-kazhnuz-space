<?php include(dirname(__FILE__).'/components/header.php'); ?>

<div class="container-blog" id="page-content">
    <main class="mainpane">
      <!-- TODO: élément principale -->
      <article class="card card-info" role="article">
        <div class="card-header">Bienvenue sur le blog créatif de Kazhnuz</div>
				<div class="card-body"><p>Bonjour, et bienvenue sur Kazhnuz Space, mon blog créatif ! C'est ici que je poste toute mes créations, qu'elles soient graphiques, textuelles, voir vidéoludiques.</p>
				  <p>J'espère que vous passerez un bon moment sur ce blog, à lire mes petites bétises :)</p>
				</div>
			</article>

      <section id="home-flex"><?php include(dirname(__FILE__).'/components/gallery.php'); ?></section>

        <p class="align-center"><img class="responsive" style="margin:auto;max-width:100%;height:auto;" src="https://kazhnuz.space/data/medias/incoming____by_kazhnuz-d5o055l.gif" /></p>
    </main>

    <section class="sidebar">
      <aside class="card card-primary">
        <div class="card-header">Cémwah</div>
	      <div class="card-body">
	        <?php $plxShow->staticInclude('003') ?>
	      </div>
      </aside>


	  <aside class="card card-primary">
      <div class="card-header">Calendrier républicain</div>
	    <div class="card-body">
        <p class="align-center"><?php include(dirname(__FILE__).'/components/republicandate.php'); echo gregorian2FrenchDateString(date('n'), date('j'), date('Y'));?></p>
        <p class="align-center"><em><a class="btn btn-primary" href="https://framagit.org/Kazhnuz/republican-calander-php">Code Source</a></em></p>
      </div>
    </aside>

    <?php include(dirname(__FILE__).'/components/sidebar.php'); ?>

    </section>

</div>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
