<?php if (!defined('PLX_ROOT')) exit; ?>

</div>

<footer>
  <ul class="social">
    <li class="social-li"><a class="social-link" href="https://kazhnuz.space/feed.php?rss" alt="RSS"><i class="fa fa-fw fa-rss"></i></a></li>
    <li class="social-li"><a class="social-link" rel="me" href="https://octodon.social/@Kazhnuz" alt="Mastodon"><i class="fa fa-fw fa-mastodon"></i></a></li>
    <li class="social-li"><a class="social-link" href="https://twitter.com/kazhnuz" alt="Twitter"><i class="fa fa-fw fa-twitter"></i></a></li>
    <li class="social-li"><a class="social-link" href="https://framasphere.org/u/kazhnuz" alt="Diaspora"><i class="fa fa-fw fa-diaspora"></i></a></li>
    <li class="social-li"><a class="social-link" href="http://kazhnuz.deviantart.com/" alt="DeviantART"><i class="fa fa-fw fa-deviantart"></i></a></li>
    <li class="social-li"><a class="social-link" href="http://framagit.org/u/kazhnuz/" alt="Gitlab"><i class="fa fa-fw fa-gitlab"></i></a></li>
    <li class="social-li"><a class="social-link" href="https://github.com/kazhnuz" alt="Github"><i class="fa fa-fw fa-github"></i></a></li>
  </ul>
  <div class="columns">
    <div class="column"><p>Ce site et tout le contenu diffusé dessus est sous licence <strong>Creative Common Attribution - Partage à l'Identique 4.0</strong> - hors mention contraire.</p>

      <p>Cette licence vous autorise à partager et copier mes travaux, tant que vous me citiez en source, et que vous autorisez la même chose pour les travaux qui en seraient dérivés. N'hésitez pas à partager ! <i style="color:#FF0000" class="fa fa-heart"></i></p>

    </div>

    <div class="column"><p>Propulsé par le moteur <a href="http://www.pluxml.org">PluXML</a>, theme réalisé avec le quadriciel <a href="https://getbootstrap.com/">Bootstrap4</a> et la banque d'icone <a href="https://forkawesome.github.io/Fork-Awesome/">Fork-Awesome</a>.</p>

    <p>Voir <a href="https://kazhnuz.space/index.php?static9/remerciements">remerciements</a> pour voir tout les outils libres utilisés pour écrire ou pour développer mes histoires.</p>
    </div>

    <div class="column"><p>Page g&eacute;n&eacute;r&eacute;e en <?php $plxShow->chrono(); ?>&nbsp; <?php $plxShow->httpEncoding() ?></p>

    <p class="align-center"><a href="<?php $plxShow->racine() ?>"><?php $plxShow->lang('HOME'); ?></a> - <a href="<?php $plxShow->urlRewrite('#top') ?>" title="<?php $plxShow->lang('GOTO_TOP') ?>"><?php $plxShow->lang('TOP') ?></a> - <a rel="nofollow" href="<?php $plxShow->urlRewrite('core/admin/'); ?>" title="<?php $plxShow->lang('ADMINISTRATION') ?>"><?php $plxShow->lang('ADMINISTRATION') ?></a></p>
    </div>

</footer>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php $plxShow->template(); ?>/js/bootstrap.min.js"></script>
    <script>$( "p:has(img)" ).addClass( "p-img" );</script>
    <script>$( ".categories a" ).addClass( "badge badge-primary" );</script>
    <script>$( ".tags:has(a)" ).removeClass( "badge badge-dark" );</script>
 </body>
</html>
