
<article class="card card-preview card-primary">
    <a href="<?php $plxShow->artUrl(); ?>" class="preview-link">
      <div class="preview-item">
        <div class="preview-content"><?php $plxShow->artContent(false); ?></div>
        <div class="preview-overlay">
          <h1 class="card-header"><?php $plxShow->artTitle(); ?></h1>
          <div class="preview-metadata">
            <div class="align-right"><time><small>Le <?php $plxShow->artDate('#num_day #month #num_year(4)'); ?></small></time></div>
            <br />
            <div class="align-center comment-text"><span class="btn btn-dark"><i class="fa fa-comments"></i> <?php echo $plxShow->plxMotor->plxRecord_arts->f('nb_com') ?> commentaire(s)</span></div>
          </div>
        </div>
      </div>
    </a>
</article>
