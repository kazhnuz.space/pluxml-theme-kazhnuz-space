<aside class="card card-info">
  <div class="card-header"><?php $plxShow->artTitle(); ?></div>
  <div class="card-body">
    <p class="align-right"><strong>Post&eacute; le :</strong> <time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>"><?php $plxShow->artDate('#num_day #month #num_year(4)'); ?></time></p>

		<?php $plxShow->artChapo('', false); ?>

    <p><strong><?php $plxShow->lang('CLASSIFIED_IN') ?> :</strong> <?php $plxShow->artCat() ?></p>

    <p><strong><?php $plxShow->lang('TAGS') ?> :</strong> <?php $plxShow->artTags() ?></p>

  </div>
</aside>
<div class="align-right social-network"><?php eval($plxShow->callHook('share_me')); ?></div>
