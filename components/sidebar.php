<section class="card card-primary">
  <h3 class="card-header"><i class="fa fa-fw fa-feed"></i>&nbsp; Publications</h3>

  <div class="card-menu">
    <?php $plxShow->lastArtList('<a class="menu-element sidebar-#art_status" href="#art_url" title="#art_title">#art_title</a>'); ?>
  </div>
</section>

<section class="card card-primary">
  <h3 class="card-header"><i class="fa fa-fw fa-folder-open"></i>&nbsp; <?php $plxShow->lang('CATEGORIES'); ?></h3>

  <div class="card-menu">
    <?php $plxShow->catList('','<a class="menu-element sidebar-#cat_status" id="catlist-#cat_id" href="#cat_url" title="#cat_name"> #cat_name <span class="menu-label label-secondary">#art_nb</span></a>', '', '1|12|11'); ?>
  </div>
</section>

<section class="card card-primary">
  <h3 class="card-header"><i class="fa fa-fw fa-calendar"></i>&nbsp; <?php $plxShow->lang('ARCHIVE'); ?></h3>

  <div class="card-menu">
    <?php $plxShow->archList('<a class="menu-element sidebar-#archives_status" href="#archives_url" title="#archives_name"> #archives_name <span class="menu-label label-secondary">#archives_nbart</span></a>'); ?>
  </div>
</section>
