<!-- En travaux -->
<section id="comments">
  <h3><?php $plxShow->artNbCom(); ?></h3>

<?php if(!defined('PLX_ROOT')) exit; ?>

	<?php if($plxShow->plxMotor->plxRecord_coms): ?>

		<?php while($plxShow->plxMotor->plxRecord_coms->loop()): # On boucle sur les commentaires ?>



<article class="panel comment">
   <div class="panel-body">
      <div class="media article-meta">
	      <div class="media-left"><?php eval($plxShow->callHook('gravatar')); ?></div>
	      <div class="media-body comment_meta">
          <author class="media-heading"><a class="nbcom" href="<?php $plxShow->ComUrl(); ?>" title="#<?php echo $plxShow->plxMotor->plxRecord_coms->i+1 ?>">#<?php echo $plxShow->plxMotor->plxRecord_coms->i+1 ?></a> - Écrit par <?php $plxShow->comAuthor('link'); ?></author>
          <time datetime="<?php $plxShow->comDate('#num_year(4)-#num_month-#num_day #hour:#minute'); ?>"> Le <?php $plxShow->comDate('#day #num_day #month #num_year(4) à #hour:#minute'); ?> </time>
        </div>
      </div>
      
      <p class="comment_body type-<?php $plxShow->comType(); ?>"><?php $plxShow->comContent(); ?></p>
      </div>
</article>



		<?php endwhile; # Fin de la boucle sur les commentaires ?>

	<?php endif; # Fin du if ?>


	<?php if($plxShow->plxMotor->plxRecord_arts->f('allow_com') AND $plxShow->plxMotor->aConf['allow_com']): ?>

<section class="card card-green">
    <div class="card-header"><?php $plxShow->lang('WRITE_A_COMMENT') ?></div>
	<div class="card-body">
	<form id="form" class="form-horizontal" action="<?php $plxShow->artUrl(); ?>#form" method="post">

		<fieldset>

    <div class="form-group">
      <label for="id_name" class="col-lg-2 control-label"><?php $plxShow->lang('NAME') ?></label>
      <div class="col-lg-10">
        <input class="form-control" name="name" id="id_name" placeholder="<?php $plxShow->comGet('name',''); ?>" type="text" size="20" maxlength="30"></input>
      </div>
    </div>

    <div class="form-group">
			<label for="id_mail" class="col-lg-2 control-label"><?php $plxShow->lang('EMAIL') ?></label>
      <div class="col-lg-10">
        <input id="id_mail" name="mail" class="form-control" placeholder="<?php $plxShow->comGet('mail',''); ?>" type="text" size="20"></input>
      </div>
    </div>

    <div class="form-group">
			<label for="id_site" class="col-lg-2 control-label"><?php $plxShow->lang('WEBSITE') ?></label>
      <div class="col-lg-10">
        <input id="id_site" name="site" class="form-control" placeholder="<?php $plxShow->comGet('site',''); ?>" type="text" size="20"></input>
      </div>
    </div>

    <div class="form-group">
      <label for="id_content" class="col-lg-2 control-label lab_com"><?php $plxShow->lang('COMMENT') ?></label>
      <div class="col-lg-10">
        <textarea id="id_content" name="content" class="form-control" rows="6" id="textArea"><?php $plxShow->comGet('content',''); ?></textarea>
      </div>
    </div>

			<?php $plxShow->comMessage('<p class="text-red"><strong>#com_message</strong></p>'); ?>
				
			<?php if($plxShow->plxMotor->aConf['capcha']): ?>

    <div class="form-group">
      <label for="id_rep" class="col-lg-2 control-label"><?php echo $plxShow->lang('ANTISPAM_WARNING') ?></label>
      <div class="col-lg-10">
        <input class="form-control" id="id_rep" name="rep" placeholder="<?php $plxShow->comGet('name',''); ?>" type="text" size="2" maxlength="1"></input>
        <span class="help-block" style="display:block;"><?php $plxShow->capchaQ(); ?></span>
      </div>
    </div>
    <p></p>
					<?php endif; ?>

    <div class="form-group" style="text-align:right;">
      <div class="col-lg-10 col-lg-offset-2">
					<input class="btn btn-primary" type="submit" value="<?php $plxShow->lang('SEND') ?>" />
      </div>
    </div>

		</fieldset>
			
	</form>
</div>
</section>

	<?php else: ?>
<section class="panel">
	<div class="panel-body">
	<p>
		<?php $plxShow->lang('COMMENTS_CLOSED') ?>.
	</p>
  </div>
</section>

	<?php endif; # Fin du if sur l'autorisation des commentaires ?>
