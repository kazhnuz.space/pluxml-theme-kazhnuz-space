<div class="card card-primary">
  <div class="card-header"><h1><?php $plxShow->artTitle(); ?></h1></div>
  <?php $plxShow->artChapo('', false); ?>
  <div class="align-right"><time><small>Le <?php $plxShow->artDate('#num_day #month #num_year(4)'); ?></small></time></div>
  <p class="align-center"> <a href="<?php $plxShow->artUrl(); ?>" class="btn btn-primary">Lire l'article</a> </p>
</div>
