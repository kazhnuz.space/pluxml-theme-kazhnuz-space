<?php if (!defined('PLX_ROOT')) exit; ?>
<!DOCTYPE html>
<html lang="<?php $plxShow->defaultLang() ?>">
<head>
	<!-- Métadonnées -->
	<meta charset="<?php $plxShow->charset('min'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
	<title><?php $plxShow->mainTitle(); ?></title>
	<?php $plxShow->meta('description') ?>
	<?php $plxShow->meta('keywords') ?>
	<?php $plxShow->meta('author') ?>

	<!-- Favicon -->
	<link rel="icon" href="<?php $plxShow->template(); ?>/img/favicon.png" />

	<!-- Framework CSS utilisés -->
	<link href="<?php $plxShow->template(); ?>/dep/spectre/css/spectre.min.css" rel="stylesheet" media="screen"/>
	<link href="<?php $plxShow->template(); ?>/dep/fork-awesome/css/fork-awesome.min.css" rel="stylesheet" media="screen"/>

	<!-- Thème du style -->

	<link href="<?php $plxShow->template(); ?>/style.css" rel="stylesheet" media="screen"/>
	<link href="<?php $plxShow->template(); ?>/hacks.css" rel="stylesheet" media="screen"/>

	<?php $plxShow->templateCss() ?>
	<?php $plxShow->pluginsCss() ?>

	<!-- Fichier RSS -->
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />
</head>

  <body>
	<div id="wrapper">

		<header id="page-header">
		  <div class="columns">
		    <div class="column col-6">
		        <h1><img src="<?php $plxShow->template(); ?>/img/logo.png" alt="Kazhnuz"/></h1>
		    </div>
				<?php include(dirname(__FILE__).'/navbar.php'); ?>
		  </div>
		</header>
