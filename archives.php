<?php include(dirname(__FILE__).'/components/header.php'); ?>

<section id="mainsite" class="container-blog">
	<section class="sidebar">
    <aside class="card card-info">
      <div class="card-header"><i class="fa fa-fw fa-feed"></i>&nbsp; <?php echo plxDate::formatDate($plxShow->plxMotor->cible, $plxShow->lang('ARCHIVES').' #month #num_year(4)') ?></div>
      <div class="card-body"><p>Toutes les créations réalisés durant la période <?php echo plxDate::formatDate($plxShow->plxMotor->cible,'#num_year(4)') ?> </p></div>
    </aside>

	  <?php include(dirname(__FILE__).'/components/sidebar.php'); ?>
	  </section>
		<main class="mainpane">

		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php $plxShow->racine() ?>"><i class="fa fa-fw fa-home"></i>&nbsp;<?php $plxShow->lang('HOME'); ?></a></li><li class="breadcrumb-item" aria-current="page"><span class="active"><?php echo plxDate::formatDate($plxShow->plxMotor->cible, $plxShow->lang('ARCHIVES').' #month #num_year(4)') ?></span></li>
		  </ol>
		</nav>

		<?php include(dirname(__FILE__).'/components/gallery.php'); ?>

    <nav class="pagination align-center"><?php $plxShow->pagination(); ?></nav>
	</main>
</section>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
