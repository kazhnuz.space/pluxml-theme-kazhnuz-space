<?php include(dirname(__FILE__).'/components/header.php'); ?>

<section class="container-onecolumn">
  <article id="#content" class="container-typographic">
    <?php $plxShow->artContent(false); ?>
  </article>
  <?php include(dirname(__FILE__).'/components/article-navigation.php'); ?>
</section>

<section class="container-blog">
		<div class="main">
      <?php include(dirname(__FILE__).'/components/article-infos.php'); ?>

      <?php include(dirname(__FILE__).'/components/commentaires.php'); ?>
		</div>
    <div class="sidebar">
      <?php include(dirname(__FILE__).'/components/sidebar.php'); ?>
    </div>
</section>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
