<?php include(dirname(__FILE__).'/header.php'); ?>

<section id="mainsite" class="container">
	<div class="row">
		<div class="col-md-8 colonne-droite">


			<article class="panel panel-wood" role="article" id="static-page-<?php echo $plxShow->staticId(); ?>">
				<div class="panel-body">
					<h2><?php $plxShow->subTitle(); ?></h2>

					<section class="well well-paper"><?php $plxShow->staticInclude('004'); ?></section>

				</div>
			</article>
			


			<div class="panel panel-primary panel-top"><?php $plxShow->staticTitle('link'); ?></div>
			<article class="panel panel-default panel-bottom" role="article" id="static-page-<?php echo $plxShow->staticId(); ?>">
					<section class="panel-body"><?php $plxShow->staticContent(); ?></section>
			</article>

		</div>

		<aside class="col-md-4 colonne-gauche quarante-douze-sidebar">
	  <div class="panel panel-success panel-top">Cémwah</div>
	  <article class="panel panel-default panel-bottom">
	    <div class="panel-body">
	      <?php $plxShow->staticInclude('003') ?>		
	    </div>
	  </article>

	  <div class="panel panel-danger panel-top">Calendrier républicain</div>
	  <article class="panel panel-default panel-bottom">
	    <div class="panel-body">
        <p class="center-it"><?php include(dirname(__FILE__).'/republicandate.php'); echo gregorian2FrenchDateString(date('n'), date('j'), date('Y'));?></p>
        <p class="center-it"><em><a class="btn btn-warning" href="https://framagit.org/Kazhnuz/republican-calander-php">Code Source</a></em></p>		
      </div>
    </article>
      
	  <div class="panel panel-rock panel-top">I N C O M I N G</div>
	  <article class="panel panel-default panel-bottom">
        <img class="responsive" style="width:100%;height:auto;" src="https://kazhnuz.space/data/medias/incoming____by_kazhnuz-d5o055l.gif" />
    </article>

</aside>

	</div>
</section>

<?php include(dirname(__FILE__).'/footer.php'); ?>
