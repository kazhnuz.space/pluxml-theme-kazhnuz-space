<?php include(dirname(__FILE__).'/components/header.php'); ?>

<section class="container-blog">
		<div class="main">
      <?php include(dirname(__FILE__).'/components/article-content.php'); ?>

      <?php include(dirname(__FILE__).'/components/commentaires.php'); ?>
		</div>
    <div class="sidebar">
      <?php include(dirname(__FILE__).'/components/sidebar.php'); ?>
    </div>
</section>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
