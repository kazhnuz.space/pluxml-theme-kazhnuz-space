<?php include(dirname(__FILE__).'/components/header.php'); ?>

<section id="mainsite" class="container-blog">
		<div class="mainpane">

			<article class="card card-primary" role="article" id="static-page-<?php echo $plxShow->staticId(); ?>">
			  <div class="card-header"><?php $plxShow->staticTitle('link'); ?></div>
				<div class="card-body">

					<section><?php $plxShow->staticContent(); ?></section>

				</div>
			</article>

		</div>
    <div class="sidebar">
		  <?php include(dirname(__FILE__).'/components/sidebar.php'); ?>
    </div>
</section>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
