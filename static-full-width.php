<?php include(dirname(__FILE__).'/components/header.php'); ?>

<section id="mainsite" class="container">
	<div class="row">
		<div class="col-md-12 colonne-droite">

      <article class="card card-primary" role="article" id="static-page-<?php echo $plxShow->staticId(); ?>">
			  <div class="card-header"><?php $plxShow->staticTitle('link'); ?></div>
				<div class="card-body">
					<section><?php $plxShow->staticContent(); ?></section>
				</div>
			</article>


		</div>

	</div>
</section>

<?php include(dirname(__FILE__).'/components/footer.php'); ?>
